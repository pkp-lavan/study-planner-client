﻿
namespace StudyPlanner
{
    partial class MainView
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitMain = new System.Windows.Forms.SplitContainer();
            this.splitSide = new System.Windows.Forms.SplitContainer();
            this.lblWeek = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.flowNavButtons = new System.Windows.Forms.FlowLayoutPanel();
            this.btnHome = new System.Windows.Forms.Button();
            this.btnTaskCalendar = new System.Windows.Forms.Button();
            this.btnTasks = new System.Windows.Forms.Button();
            this.btnTimetable = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).BeginInit();
            this.splitMain.Panel1.SuspendLayout();
            this.splitMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitSide)).BeginInit();
            this.splitSide.Panel1.SuspendLayout();
            this.splitSide.Panel2.SuspendLayout();
            this.splitSide.SuspendLayout();
            this.flowNavButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitMain
            // 
            this.splitMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitMain.IsSplitterFixed = true;
            this.splitMain.Location = new System.Drawing.Point(0, 0);
            this.splitMain.Name = "splitMain";
            // 
            // splitMain.Panel1
            // 
            this.splitMain.Panel1.Controls.Add(this.splitSide);
            this.splitMain.Size = new System.Drawing.Size(1295, 797);
            this.splitMain.SplitterDistance = 240;
            this.splitMain.TabIndex = 0;
            // 
            // splitSide
            // 
            this.splitSide.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitSide.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitSide.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitSide.IsSplitterFixed = true;
            this.splitSide.Location = new System.Drawing.Point(0, 0);
            this.splitSide.Name = "splitSide";
            this.splitSide.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitSide.Panel1
            // 
            this.splitSide.Panel1.Controls.Add(this.lblWeek);
            this.splitSide.Panel1.Controls.Add(this.lblDate);
            // 
            // splitSide.Panel2
            // 
            this.splitSide.Panel2.Controls.Add(this.flowNavButtons);
            this.splitSide.Size = new System.Drawing.Size(240, 797);
            this.splitSide.SplitterDistance = 180;
            this.splitSide.TabIndex = 0;
            // 
            // lblWeek
            // 
            this.lblWeek.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWeek.AutoSize = true;
            this.lblWeek.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblWeek.Location = new System.Drawing.Point(76, 141);
            this.lblWeek.Name = "lblWeek";
            this.lblWeek.Size = new System.Drawing.Size(85, 25);
            this.lblWeek.TabIndex = 1;
            this.lblWeek.Text = "2 savaitė";
            // 
            // lblDate
            // 
            this.lblDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblDate.Location = new System.Drawing.Point(8, 90);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(225, 30);
            this.lblDate.TabIndex = 0;
            this.lblDate.Text = "2021 m. gruodžio 14 d.";
            // 
            // flowNavButtons
            // 
            this.flowNavButtons.Controls.Add(this.btnHome);
            this.flowNavButtons.Controls.Add(this.btnTaskCalendar);
            this.flowNavButtons.Controls.Add(this.btnTasks);
            this.flowNavButtons.Controls.Add(this.btnTimetable);
            this.flowNavButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowNavButtons.Location = new System.Drawing.Point(0, 0);
            this.flowNavButtons.Name = "flowNavButtons";
            this.flowNavButtons.Size = new System.Drawing.Size(238, 611);
            this.flowNavButtons.TabIndex = 0;
            // 
            // btnHome
            // 
            this.btnHome.Location = new System.Drawing.Point(3, 3);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(232, 40);
            this.btnHome.TabIndex = 0;
            this.btnHome.Text = "Pagrindinis";
            this.btnHome.UseVisualStyleBackColor = true;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // btnTaskCalendar
            // 
            this.btnTaskCalendar.Location = new System.Drawing.Point(3, 49);
            this.btnTaskCalendar.Name = "btnTaskCalendar";
            this.btnTaskCalendar.Size = new System.Drawing.Size(232, 40);
            this.btnTaskCalendar.TabIndex = 3;
            this.btnTaskCalendar.Text = "Užduočių kalendorius";
            this.btnTaskCalendar.UseVisualStyleBackColor = true;
            this.btnTaskCalendar.Click += new System.EventHandler(this.btnTaskCalendar_Click);
            // 
            // btnTasks
            // 
            this.btnTasks.Location = new System.Drawing.Point(3, 95);
            this.btnTasks.Name = "btnTasks";
            this.btnTasks.Size = new System.Drawing.Size(232, 40);
            this.btnTasks.TabIndex = 1;
            this.btnTasks.Text = "Užduotys";
            this.btnTasks.UseVisualStyleBackColor = true;
            this.btnTasks.Click += new System.EventHandler(this.btnTasks_Click);
            // 
            // btnTimetable
            // 
            this.btnTimetable.Location = new System.Drawing.Point(3, 141);
            this.btnTimetable.Name = "btnTimetable";
            this.btnTimetable.Size = new System.Drawing.Size(232, 40);
            this.btnTimetable.TabIndex = 2;
            this.btnTimetable.Text = "Tvarkaraštis";
            this.btnTimetable.UseVisualStyleBackColor = true;
            this.btnTimetable.Click += new System.EventHandler(this.btnTimetable_Click);
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1295, 797);
            this.Controls.Add(this.splitMain);
            this.Name = "MainView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Study Planner";
            this.splitMain.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).EndInit();
            this.splitMain.ResumeLayout(false);
            this.splitSide.Panel1.ResumeLayout(false);
            this.splitSide.Panel1.PerformLayout();
            this.splitSide.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitSide)).EndInit();
            this.splitSide.ResumeLayout(false);
            this.flowNavButtons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitMain;
        private System.Windows.Forms.SplitContainer splitSide;
        private System.Windows.Forms.FlowLayoutPanel flowNavButtons;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Button btnTasks;
        private System.Windows.Forms.Button btnTimetable;
        private System.Windows.Forms.Button btnTaskCalendar;
        private System.Windows.Forms.Label lblWeek;
        private System.Windows.Forms.Label lblDate;
    }
}

