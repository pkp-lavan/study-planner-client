﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudyPlanner
{
    public partial class MainView : Form
    {
        private UserControl CurrentControl { get; set; }

        private int StudyWeek { get; set; }

        public MainView()
        {
            InitializeComponent();

            InitControls();
        }

        private void InitControls()
        {
            DateTime currentDate = DateTime.Now;

            StudyWeek = GetWeek(currentDate);

            lblDate.Text = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day).ToString("yyyy MMMMM d") + " d.";
            lblDate.Location = new Point((splitSide.Panel1.Width - lblDate.Width) / 2, lblDate.Location.Y);

            lblWeek.Text = StudyWeek + " savaitė";
            lblWeek.Location = new Point((splitSide.Panel1.Width - lblWeek.Width) / 2, lblWeek.Location.Y);
        }

        private void btnHome_Click(object sender, EventArgs e)
        {

        }

        private void btnTaskCalendar_Click(object sender, EventArgs e)
        {

        }

        private void btnTasks_Click(object sender, EventArgs e)
        {

        }

        private void btnTimetable_Click(object sender, EventArgs e)
        {

        }

        private void ChangeCurrentControl(UserControl control)
        {
            splitMain.Panel2.Controls.Clear();
            CurrentControl?.Dispose();
            control.Dock = DockStyle.Fill;
            CurrentControl = control;
            splitMain.Panel2.Controls.Add(CurrentControl);
        }

        private int GetWeek(DateTime date)
        {
            DateTime weekOneStart = StartOfWeek(new DateTime(2021, 9, 1));
            DateTime weekTwoStart;

            int c = 0;

            for (int i = 0; i < 1000; i++)
            {
                c++;
                weekTwoStart = weekOneStart.AddDays(7);
                if (weekOneStart <= date && date < weekTwoStart)
                {
                    return c % 2 != 0 ? 1 : 2;
                }
                weekOneStart = weekTwoStart;
            }

            return 1;
        }

        private DateTime StartOfWeek(DateTime dt)
        {
            int diff = (7 + (dt.DayOfWeek - DayOfWeek.Monday)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }
    }
}
