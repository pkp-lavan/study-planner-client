﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudyPlanner.Models
{
    public class Time : IComparable<Time>
    {
        public Time(int hours, int minutes)
        {
            this.Hours = hours;
            this.Minutes = minutes;
        }

        public int Hours { get; set; }

        public int Minutes { get; set; }
        
        [JsonIgnore]
        public int TotalMinutes
        {
            get 
            { 
                return Hours*60+Minutes; 
            }
        }

        public int CompareTo(Time other)
        {
            if(this.TotalMinutes > other.TotalMinutes)
            {
                return 1;
            }
            else if(this.TotalMinutes < other.TotalMinutes)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }

        public override string ToString()
        {
            return Hours + ":" + Minutes;
        }
    }
}
