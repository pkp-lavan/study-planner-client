﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudyPlanner.Models
{
    public class TimetableEntry : IComparable<TimetableEntry>
    {
        public DayOfWeek DayOfWeek { get; set; }

        public int Nr { get; set; }

        public Subject Subject { get; set; }

        public Time Time { get; set; }

        public int Week { get; set; }

        public int CompareTo(TimetableEntry other)
        {
            int d = this.DayOfWeek.CompareTo(other.DayOfWeek);
            if (d != 0)
            {
                return d;
            }
            int n = this.Nr.CompareTo(other.Nr);
            if (n != 0)
            {
                return n;
            }
            return this.Week.CompareTo(other.Week);
        }
    }
}
