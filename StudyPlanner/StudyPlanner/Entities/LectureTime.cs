﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudyPlanner.Models
{
    public static class LectureTime
    {
        public static Dictionary<int, Time> LectureTimePairs = new Dictionary<int, Time>()
        {
            { 1, new Time(8, 30)},
            { 2, new Time(10, 20)},
            { 3, new Time(12, 10)},
            { 4, new Time(14, 30)},
            { 5, new Time(16, 20)},
            { 6, new Time(18, 10)},
            { 7, new Time(19, 55)}
        };
    }
}
