﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudyPlanner.Models
{
    public class Task
    {
        public bool IsDone { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public TaskType TaskType { get; set; }

        public Subject Subject { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
